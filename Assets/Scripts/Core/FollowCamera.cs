﻿using System.Collections;
using System.Collections.Generic;
using RPG.Control;
using RPG.Movement;
using UnityEngine;


namespace RPG.Core
{
    public class FollowCamera : MonoBehaviour
    {
        private Transform cameraFollowTarget;

        private void Start()
        {
            cameraFollowTarget = FindObjectOfType<PlayerController>().transform;
        }

        // Update is called once per frame
        void LateUpdate()
        {
            transform.position = cameraFollowTarget.position;
        }
    }
}
