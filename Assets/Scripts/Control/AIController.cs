﻿using System.Collections;
using System.Collections.Generic;
using RPG.Combat;
using RPG.Core;
using RPG.Movement;
using RPG.Resources;
using UnityEngine;

namespace RPG.Control
{
    public class AIController : MonoBehaviour
    {
        //Control player speed in different situation like
        //Patrolling, Injured, Stoned and etc.
        [Range(0,1)]
        [SerializeField] private float patrolSpeedFraction=0.2f;

        [SerializeField] private float chaseDistance = 5f;

        [SerializeField] private float suspicionTime = 3f;
        [SerializeField] private float waypointDwellTime = 3f;
        [SerializeField] private float waypointTollerance = 1.0f;

        [SerializeField] private PatrolPath patrolPath;


        private GameObject player;
        private Fighter fighter;
        private Health health;
        private Mover mover;
        private ActionSchedular actionSchedular;

        private Vector3 guardPosition;
        private float timeSinceLastSawPlayer=Mathf.Infinity;
        private float timeSinceArrivedWaypoint=Mathf.Infinity;
        private int currentWaypointIndex = 0;


        // Start is called before the first frame update
        void Start()
        {
            actionSchedular = GetComponent<ActionSchedular>();
            mover = GetComponent<Mover>();
            health = GetComponent<Health>();
            fighter = GetComponent<Fighter>();
            player=GameObject.FindGameObjectWithTag("Player");

            guardPosition = transform.position;
        }

        // Update is called once per frame
        void Update()
        {
            if(health.IsDead)    return;

            CheckToAttack();
        }

        private void CheckToAttack()
        {
            if (InAttackRange())
            {
                //When the player is in range the guard start attacking to it.
                AttackBehaviour();
            }
            else if (timeSinceLastSawPlayer<=suspicionTime)
            {
                //Suspicious state implementation
                SuspicionBehaviour();
            }
            else
            {
                //When the player is not in range the guard will go back to guard position
                PatrolBehaviour();
            }

            UpdateTimers();
        }

        private void UpdateTimers()
        {
            timeSinceLastSawPlayer += Time.deltaTime;
            timeSinceArrivedWaypoint += Time.deltaTime;
        }

        private void PatrolBehaviour()
        {
            Vector3 nextPosition = guardPosition;

            if (patrolPath)
            {
                if (AtWaypoint())
                {
                    timeSinceArrivedWaypoint = 0;
                    CycleWaypoint();
                }

                nextPosition = GetCurrentWaypoint();
            }

            if (timeSinceArrivedWaypoint > waypointDwellTime)
            {
                mover.StartMoveAction(nextPosition,patrolSpeedFraction);
            }
        }

        private bool AtWaypoint()
        {
            return Vector3.Distance
                       (transform.position, GetCurrentWaypoint()) < waypointTollerance;
        }

        private void CycleWaypoint()
        {
            timeSinceArrivedWaypoint = 0;
            currentWaypointIndex=patrolPath.GetNextIndex(currentWaypointIndex);
        }

        private Vector3 GetCurrentWaypoint()
        {
            return patrolPath.GetWaypontPosition(currentWaypointIndex);
        }

        private void SuspicionBehaviour()
        {
            actionSchedular.CancelCurrentAction();
        }

        private void AttackBehaviour()
        {
            timeSinceLastSawPlayer = 0;
            fighter.Attack(player.gameObject);
        }

        private bool InAttackRange()
        {
            return Vector3.Distance(player.transform.position, transform.position) <= chaseDistance;
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(transform.position,chaseDistance);
        }
    }


}

