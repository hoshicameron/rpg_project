﻿using System.Collections;
using System.Collections.Generic;
using RPG.Core;
using RPG.Saving;
using RPG.Stats;
using UnityEngine;
using UnityEngine.Serialization;

namespace RPG.Resources
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Animator))]
    public class Health : MonoBehaviour,ISaveable
    {
        [SerializeField] private float regenerateHealth = 70f;

        private float healthPoints = -1f;
        private Animator anim;
        private int deathParamID;
        private bool isDead = false;
        public bool IsDead
        {
            get { return isDead; }
            set { isDead = value; }
        }
        private void Awake()
        {
            anim = GetComponent<Animator>();
            deathParamID = Animator.StringToHash("Die");
        }
        private void Start()
        {
            GetComponent<BaseStats>().OnLevelUp += RegenerateHealth;
            if(healthPoints<0)
                healthPoints = GetComponent<BaseStats>().GetStat(Stat.Health);
        }

        public void TakeDamage(GameObject instigator,float damage)
        {
            if(IsDead)    return;

            print($"{gameObject.name} took damage: {damage}");

            healthPoints = Mathf.Max(healthPoints - damage, 0);
            if (healthPoints <= 0)
            {
                Die();
                AwardExperience(instigator);
            }
        }

        public float GetHealthPoints()
        {
            return healthPoints;
        }

        public float GetMaxHealthPoints()
        {
            return GetComponent<BaseStats>().GetStat(Stat.Health);
        }
        public float GetPercentage()
        {
            return 100 * (healthPoints / GetComponent<BaseStats>().GetStat(Stat.Health));
        }
        private void Die()
        {
            anim.SetTrigger(deathParamID);
            IsDead = true;
            GetComponent<ActionSchedular>().CancelCurrentAction();
            //Disable gameObject collider to avoid raycasting
            DisableCollider();
        }

        private void AwardExperience(GameObject instigator)
        {
            Experience experience=instigator.GetComponent<Experience>();
            if(!experience)    return;
            float exPoint = GetComponent<BaseStats>().GetStat(Stat.ExperienceReward);
            experience.GainExperience(exPoint);
        }

        private void RegenerateHealth()
        {
            var regenerateHealthpoints=GetComponent<BaseStats>().GetStat(Stat.Health)*(regenerateHealth/100f);
            healthPoints = Mathf.Max(healthPoints,regenerateHealthpoints);
        }
        private void DisableCollider()
        {
            GetComponent<CapsuleCollider>().enabled=false;
        }

        public object CaptureState()
        {
            return this.healthPoints;
        }

        public void RestoreState(object state)
        {
            this.healthPoints = (float) state;

            if (healthPoints <= 0)
            {
                Die();
            } else
                isDead = false;
        }
    }

}

