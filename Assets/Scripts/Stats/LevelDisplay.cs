﻿using System.Collections;
using System.Collections.Generic;
using RPG.Saving;
using TMPro;
using UnityEngine;

namespace RPG.Stats
{
    public class LevelDisplay : MonoBehaviour
    {
        private BaseStats baseStats;
        private TextMeshProUGUI textMeshProUgui;

        private void Awake()
        {
            baseStats = GameObject.FindWithTag("Player").GetComponent<BaseStats>();
        }

        private void Start()
        {
            textMeshProUgui = GetComponent<TextMeshProUGUI>();
        }

        private void Update()
        {
            UpdateLevel();
        }

        private void UpdateLevel()
        {
            textMeshProUgui.text = baseStats.GetLevel().ToString();
        }
    }

}

