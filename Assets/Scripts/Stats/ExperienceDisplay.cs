﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace RPG.Stats
{
    public class ExperienceDisplay : MonoBehaviour
    {
        private Experience experience;
        private TextMeshProUGUI textMeshProUgui;

        private void Awake()
        {
            textMeshProUgui = GetComponent<TextMeshProUGUI>();
            experience = GameObject.FindWithTag("Player").GetComponent<Experience>();
        }

        private void Update()
        {
            UpdateExperience();
        }

        private void UpdateExperience()
        {
            textMeshProUgui.text = $"{experience.ExperiencePoints:0}";
        }
    }

}

