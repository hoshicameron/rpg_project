﻿using System.Collections;
using System.Collections.Generic;
using RPG.Combat;
using RPG.Core;
using RPG.Movement;
using RPG.Resources;
using UnityEngine;

namespace RPG.Control
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Mover))]
    [RequireComponent(typeof(Fighter))]
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private LayerMask enemyLayer;
        private Mover mover;
        private Camera camera1;
        private Fighter fighter;
        private Health health;

        // Start is called before the first frame update
        void Start()
        {
            health = GetComponent<Health>();
            mover = GetComponent<Mover>();
            fighter = GetComponent<Fighter>();
            camera1 = Camera.main;
        }

        // Update is called once per frame
        void Update()
        {
            if(health.IsDead)    return;

            if(InteractWithCombat())    return;
            if(InteractWithMovement())  return;

        }

        private bool InteractWithCombat()
        {
            RaycastHit hit;
            if (Physics.Raycast(GetMouseRay(), out hit, enemyLayer))
            {
                CombatTarget target = hit.transform.GetComponent<CombatTarget>();
                if (target)
                {
                    if(Input.GetMouseButton(0))
                        fighter.Attack(target.gameObject);
                    //return true because if we want to change cursor icon we need to return
                    //true even when mouse hover over the enemy
                    return true;
                }
            }
            return false;
        }

        private bool InteractWithMovement()
        {
           if (Physics.Raycast(GetMouseRay(), out var hit))
            {
                if (Input.GetMouseButton(0))
                    mover.StartMoveAction(hit.point,1f);//Todo Implement speed fraction based on player condition

                return true;
            }

            return false;
        }

        private Ray GetMouseRay()
        {
            return camera1.ScreenPointToRay(Input.mousePosition);
        }
    }

}

