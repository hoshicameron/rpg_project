﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using RPG.Core;
using RPG.Movement;
using RPG.Resources;
using RPG.Saving;
using RPG.Stats;
using UnityEngine;
using UnityEngine.Serialization;

namespace RPG.Combat
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Mover))]
    [RequireComponent(typeof(Animator))]
    public class Fighter : MonoBehaviour, IAction,ISaveable,IModifierProvider
    {
        [Range(0,1)]
        [SerializeField] private float attackSpeedFraction = 0.7f;
        [SerializeField] private float timeBetweenAttacks = 1.0f;
        [FormerlySerializedAs("handTransform")] [SerializeField] private Transform rightHandTransform = null;
        [FormerlySerializedAs("LeftHandTransform")] [SerializeField] private Transform leftHandTransform = null;
        [SerializeField] private Weapon defaultWeapon=null;

        private Health target;
        private Mover mover;
        private Animator anim;
        private int attackParamID;
        private int stopAttackParamID;
        private float timeSinceLastAttack=Mathf.Infinity;
        private Weapon currentWeapon=null;

        private void Start()
        {
            anim = GetComponent<Animator>();
            mover = GetComponent<Mover>();
            attackParamID = Animator.StringToHash("Attack");
            stopAttackParamID = Animator.StringToHash("StopAttack");

            if(currentWeapon==null)
                EquipWeapon(defaultWeapon);
        }

        private void Update()
        {
            timeSinceLastAttack += Time.deltaTime;

            if (CantAttack()) return;

            MoveTowardTheTarget();
        }

        public void EquipWeapon(Weapon weapon)
        {
            if(!weapon) {Debug.LogError("No default weapon");};
            currentWeapon = weapon;
            if (anim == null)
                anim = GetComponent<Animator>();
            weapon.Spawn(rightHandTransform,leftHandTransform,
                anim);
        }

        public Health GetTarget()
        {
            return target;
        }
        private bool CantAttack()
        {
            if (!target) return true;
            if (target.IsDead) return true;
            return false;
        }

        private void MoveTowardTheTarget()
        {
            if (!IsInRange())
                mover.MoveTo(target.transform.position,attackSpeedFraction);
            else
            {
                mover.Cancel();
                AttackBehaviour();
            }
        }

        private void AttackBehaviour()
        {
            transform.LookAt(target.transform);
            if (timeSinceLastAttack >= timeBetweenAttacks)
            {
                TriggerAttack();
                timeSinceLastAttack = 0f;
            }
        }

        private void TriggerAttack()
        {
            anim.ResetTrigger(stopAttackParamID);
            anim.SetTrigger(attackParamID);
        }

        private bool IsInRange()
        {
            bool isInRange = Vector3.Distance(transform.position, target.transform.position) < currentWeapon.WeaponRange;
            return isInRange;
        }

        public void Attack(GameObject combatTarget)
        {
            if (combatTarget)
            {
                GetComponent<ActionSchedular>().StartAction(this);
                target = combatTarget.GetComponent<Health>();
            }
        }

        public void Cancel()
        {
            StopAttackTrigger();
            target = null;
            mover.Cancel();
        }

        private void StopAttackTrigger()
        {
            anim.ResetTrigger(attackParamID);
            anim.SetTrigger(stopAttackParamID);
        }

        public IEnumerable<float> GetAdditiveModifier(Stat stat)
        {
            if (stat == Stat.Damage)
            {
                yield return currentWeapon.WeaponDamage;
            }
        }

        //Animation Event
        void Hit()
        {
            if(!target)    return;

            float damage = GetComponent<BaseStats>().GetStat(Stat.Damage);
            if (currentWeapon.HasProjectile())
            {
                currentWeapon.LunchProjectile(rightHandTransform,leftHandTransform,target,gameObject,damage);
            } else
            {

                target.TakeDamage(gameObject,damage);
            }
        }

        //Animation Event
        void Shoot()
        {
            Hit();
        }

        public object CaptureState()
        {
            return currentWeapon.name;
        }

        public void RestoreState(object state)
        {
            string weaponName = (string) state;
            Weapon weapon = UnityEngine.Resources.Load<Weapon>(weaponName);
            EquipWeapon(weapon);
        }


    }


}
