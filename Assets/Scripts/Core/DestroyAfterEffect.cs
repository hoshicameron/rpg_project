﻿using UnityEngine;

namespace RPG.Core
{
    public class DestroyAfterEffect : MonoBehaviour
    {
        [SerializeField] private GameObject targetToDestroy=null;
        private ParticleSystem particleSystem1;

        private void Start()
        {
            particleSystem1 = GetComponent<ParticleSystem>();
        }

        private void Update()
        {
            if (!particleSystem1.IsAlive())
            {
                if(targetToDestroy)
                    Destroy(targetToDestroy);
                else
                    Destroy(gameObject);
            }
        }
    }
}
