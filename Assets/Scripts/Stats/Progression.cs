﻿using System.Collections.Generic;
using RPG.Resources;

namespace RPG.Stats
{
    using UnityEngine;

    [CreateAssetMenu(fileName = "Progression", menuName ="Stats/New Progression", order = 0)]
    public class Progression : ScriptableObject
    {
        [SerializeField] private ProgressionCharacterClass[] characterClasses=null;

        private Dictionary<CharacterClass, Dictionary<Stat, float[]>> lookupTable=null;
        public float GetStat(Stat stat,CharacterClass characterClass, int level)
        {
            BuildLookup();
            float[] levels=lookupTable[characterClass][stat];
            if (levels.Length < level)
                return 0;
            return levels[level - 1];
        }

        public int GetLevels(Stat stat, CharacterClass characterClass)
        {
            BuildLookup();

            float[] levels=lookupTable[characterClass][stat];
            return levels.Length;
        }

        private void BuildLookup()
        {
            if(lookupTable!=null)    return;

            lookupTable=new Dictionary<CharacterClass, Dictionary<Stat, float[]>>();

            foreach (ProgressionCharacterClass progressionClass in characterClasses)
            {
                var statLookupTable=new Dictionary<Stat,float[]>();
                foreach (var progressionStats in progressionClass.Stats)
                {
                    statLookupTable[progressionStats.Stats] = progressionStats.Levels;
                }

                lookupTable[progressionClass.Character] = statLookupTable;
            }
        }

        [System.Serializable]
        class ProgressionCharacterClass
        {
            [SerializeField] private CharacterClass character;
            [SerializeField] private ProgressionStats[] stats;

            public CharacterClass Character => character;

            public ProgressionStats[] Stats
            {
                get => stats;
                set => stats = value;
            }
        }
    }

    [System.Serializable]
    class ProgressionStats
    {
        [SerializeField] private Stat stats;
        [SerializeField] private float[] levels;

        public Stat Stats
        {
            get => stats;
            set => stats = value;
        }

        public float[] Levels
        {
            get => levels;
            set => levels = value;
        }
    }
}
