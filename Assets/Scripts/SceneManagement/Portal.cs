﻿using System.Collections;
using System.Collections.Generic;
using RPG.Saving;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

namespace RPG.SceneManagement
{
   public class Portal : MonoBehaviour
   {
      public enum DestinationPortal
      {
         A,
         B,
         C,
         D,
         E
      };

      [SerializeField] private int sceneToLoad = -1;
      [SerializeField] private DestinationPortal destinationPortal;
      [SerializeField] private float fadeOutTime=1.0f;
      [SerializeField] private float fadeInTime=2.0f;
      [SerializeField] private float fadeWaitTime=0.5f;

      private Transform spawnPoint;
      private int playerLayer;
      private Fader fader;

      private void Awake()
      {
         playerLayer = LayerMask.NameToLayer("Player");
         spawnPoint = transform.GetChild(0);
      }

      private void Start()
      {
         fader = FindObjectOfType<Fader>();
      }

      private void OnTriggerEnter(Collider other)
      {
         if(other.gameObject.layer!=playerLayer)   return;

         StartCoroutine(nameof(Transition));
      }

      private IEnumerator Transition()
      {
         if (sceneToLoad < 0)
         {
            Debug.LogError("Scene To Load Not Set");
            yield break;
         }
         DontDestroyOnLoad(gameObject);

         yield return fader.FadeOut(fadeOutTime);

         SavingWrapper wrapper = FindObjectOfType<SavingWrapper>();
         wrapper.Save();

         yield return SceneManager.LoadSceneAsync(sceneToLoad);

         wrapper.Load();

         Portal otherPortal = GetOtherPortal();
         UpdatePlayer(otherPortal);

         wrapper.Save();

         yield return new WaitForSeconds(fadeWaitTime);
         yield return fader.FadeIn(fadeInTime);

         Destroy(gameObject);
      }

      private void UpdatePlayer(Portal otherPortal)
      {
         GameObject player=GameObject.FindGameObjectWithTag("Player");
         player.GetComponent<NavMeshAgent>().enabled = false;
         player.transform.position = otherPortal.spawnPoint.position;
         player.transform.rotation = otherPortal.spawnPoint.rotation;
         player.GetComponent<NavMeshAgent>().enabled = true;
      }

      private Portal GetOtherPortal()
      {
         var portals = GameObject.FindObjectsOfType<Portal>();
         foreach (var portal in portals)
         {
            if (portal==this)   continue;

            if(portal.destinationPortal==this.destinationPortal)
               return portal;
         }

         return null;
      }
   }

}

