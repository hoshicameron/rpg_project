﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace RPG.Resources
{
    public class HealthDisplay : MonoBehaviour
    {
        private Health health;
        private TextMeshProUGUI textMeshProUgui;

        private void Start()
        {
            textMeshProUgui = GetComponent<TextMeshProUGUI>();
        }

        private void Awake()
        {
            health = GameObject.FindWithTag("Player").GetComponent<Health>();
        }

        private void Update()
        {
            UpdateHealth();
        }

        public void UpdateHealth()
        {
            textMeshProUgui.text = $"{health.GetHealthPoints():0}-{health.GetMaxHealthPoints():0}";
        }
    }

}

