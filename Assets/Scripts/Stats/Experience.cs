﻿using System;
using System.Collections;
using System.Collections.Generic;
using RPG.Saving;
using UnityEngine;

namespace RPG.Stats
{
    public class Experience : MonoBehaviour,ISaveable
    {
        [SerializeField] private float experiencePoints = 0;
        public float ExperiencePoints => experiencePoints;

       // public delegate void ExperienceGainedDelegate();

        public event Action onExperienceGained;

        public void GainExperience(float experience)
        {
            experiencePoints = ExperiencePoints + experience;
            onExperienceGained?.Invoke();
        }

        public object CaptureState()
        {
            return ExperiencePoints;
        }

        public void RestoreState(object state)
        {
            experiencePoints = (float) state;

        }
    }
}
