﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace RPG.Stats
{
    public class BaseStats : MonoBehaviour
    {
        [Range(1,99 )]
        [SerializeField] private int startingLevel = 1;
        [SerializeField] private CharacterClass characterClass;
        [SerializeField] private Progression progression = null;
        [SerializeField] private GameObject levelUpPrefab;

        public event Action OnLevelUp;

        private int currentLevel = 0;
        private Experience experience;

        private void Start()
        {
            experience = GetComponent<Experience>();
            currentLevel = CalculateLevel();

            if (experience)
            {
                experience.onExperienceGained += UpdateLevel;
            }
        }

        private void UpdateLevel()
        {
            int newLevel = CalculateLevel();
            if (newLevel > currentLevel)
            {
                currentLevel = newLevel;
                LevelUpEffect();
                OnLevelUp?.Invoke();
            }

        }

        private void LevelUpEffect()
        {
            Instantiate(levelUpPrefab, transform);
        }

        public float GetStat(Stat stat)
        {
           return progression.GetStat(stat,characterClass, GetLevel())+GetAdditiveModifier(stat);
        }
        public int GetLevel()
        {
            if (currentLevel < 1)
                currentLevel = CalculateLevel();
            return currentLevel;
        }

        private float GetAdditiveModifier(Stat stat)
        {
            /* foreach (IModifierProvider provider in providers)
            {
                foreach (float modifier in provider.GetAdditiveModifier(stat))
                {
                    total += modifier;
                }
            }*/
            var providers = GetComponents<IModifierProvider>();
            return providers.Sum(provider => provider.GetAdditiveModifier(stat).Sum());
        }

        private int CalculateLevel()
        {
            if (!experience) return startingLevel;

            float currentXP = experience.ExperiencePoints;
            int penultimateLevel = progression.GetLevels(Stat.ExperienceToLevelUp,characterClass);
            for (int level = 1; level < penultimateLevel; level++)
            {
                float xpToLevelUp = progression.GetStat(Stat.ExperienceToLevelUp, characterClass, level);
                if (xpToLevelUp > currentXP)
                    return level;
            }

            return penultimateLevel + 1;
        }
    }


}
