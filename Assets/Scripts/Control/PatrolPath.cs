﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Control
{
    public class PatrolPath : MonoBehaviour
    {
        private const float waypointGizmoRadius = 0.3f;
        private void OnDrawGizmos()
        {
            Gizmos.color=Color.white;

            for (int i = 0; i < transform.childCount; i++)
            {
                int j = GetNextIndex(i);
                Gizmos.DrawSphere(GetWaypontPosition(i),waypointGizmoRadius);
                Gizmos.DrawLine(GetWaypontPosition(i),GetWaypontPosition(j));
            }
        }

        public int GetNextIndex(int i)
        {

            return (i + 1)% transform.childCount;
        }

        public Vector3 GetWaypontPosition(int i)
        {
            return transform.GetChild(i).position;
        }
    }


}
