﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

namespace RPG.Cinematic
{
    public class CinematicTrigger : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if(!other.tag.Equals("Player"))    return;

            GetComponent<PlayableDirector>().Play();
            GetComponent<BoxCollider>().enabled = false;

        }
    }
}

