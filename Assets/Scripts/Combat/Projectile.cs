﻿using System.Collections;
using System.Collections.Generic;
using RPG.Core;
using RPG.Resources;
using UnityEngine;

namespace RPG.Combat
{
    public class Projectile : MonoBehaviour
    {
        [SerializeField] private GameObject impactVfxPrefab = null;
        [SerializeField] private GameObject[] destroyOnHit=null;

        [SerializeField] private float speed = 2.0f;
        [SerializeField] private bool isHoming = false;
        [SerializeField] private float maxLifeTime = 10f;
        [SerializeField] private float lifeAfterImpact;

        private float damage = 0;

        private CapsuleCollider targetCapsule;
        private Health target=null;
        private GameObject instigator;


        public Health Target
        {
            get { return target; }
            set { target = value;  Destroy(gameObject,maxLifeTime); }
        }

        public float Damage
        {
            get { return damage; }
            set { damage = value; }
        }

        public GameObject Instigator
        {
            get { return instigator; }
            set { instigator = value; }
        }

        private void Start()
        {
            if(Target)
                targetCapsule = Target.GetComponent<CapsuleCollider>();
            transform.LookAt(GetAimLocation());
        }

        private void Update()
        {
            if (!Target) return;

            if(!target.IsDead && isHoming)
                transform.LookAt(GetAimLocation());

            transform.Translate(Vector3.forward * Time.deltaTime * speed);
        }

        private Vector3 GetAimLocation()
        {
            if (!targetCapsule)
                return Target.transform.position;
            return Target.transform.position + Vector3.up * targetCapsule.height * 0.5f;
        }

        private void OnTriggerEnter(Collider other)
        {
            if(!other.TryGetComponent(typeof(Health), out var component)) return;

            other.GetComponent<Health>().TakeDamage(instigator,damage);
            GetComponent<Collider>().enabled = false;
            if (impactVfxPrefab)
                Instantiate(impactVfxPrefab, GetAimLocation(),transform.rotation);

            foreach (var toDestroy in destroyOnHit)
            {
               Destroy(toDestroy);
            }
            Destroy(gameObject,lifeAfterImpact);
        }
    }


}
