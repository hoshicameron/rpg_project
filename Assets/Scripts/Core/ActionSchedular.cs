﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Core
{
    public class ActionSchedular : MonoBehaviour
    {
        private IAction currentAction;
        public void StartAction(IAction action)
        {
            //When new action and old one is same then don't cancel.
            if (currentAction==action)    return;
            //If currentAction is not null then cancel it
            currentAction?.Cancel();

            currentAction = action;
        }

        public void CancelCurrentAction()
        {
            StartAction(null);
        }
    }

}

