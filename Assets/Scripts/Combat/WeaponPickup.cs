﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.Combat
{
    public class WeaponPickup : MonoBehaviour
    {
        [SerializeField] private Weapon weapon;
        [SerializeField] private float respawnTime = 5.0f;
        private int playerLayer;

        private void Start()
        {
            playerLayer = LayerMask.NameToLayer("Player");
        }

        private void OnTriggerEnter(Collider other)
        {
            if(other.gameObject.layer!=playerLayer)    return;

            other.GetComponent<Fighter>().EquipWeapon(weapon);
            StartCoroutine(nameof(HideForSeconds),respawnTime);
        }

        IEnumerator HideForSeconds(float seconds)
        {
            ShowPickup(false);
            yield return new WaitForSeconds(seconds);
            ShowPickup(true);
        }

        private void ShowPickup(bool shoudShow)
        {
            GetComponent<Collider>().enabled = shoudShow;
            foreach (Transform child in transform)
            {
                child.gameObject.SetActive(shoudShow);
            }
        }
    }

}
