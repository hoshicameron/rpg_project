﻿using System;
using System.Collections;
using System.Collections.Generic;
using RPG.Resources;
using TMPro;
using UnityEngine;

namespace RPG.Combat
{
    public class EnemyHealthDisplay : MonoBehaviour
    {
        private Fighter fighter;

        private void Start()
        {
            textMeshProUgui = GetComponent<TextMeshProUGUI>();
        }

        private TextMeshProUGUI textMeshProUgui;

        private void Awake()
        {
            fighter = GameObject.FindWithTag("Player").GetComponent<Fighter>();
        }

        private void Update()
        {
            UpdateHealth();
        }

        public void UpdateHealth()
        {
            if (fighter.GetTarget())
            {
                Health health = fighter.GetTarget();
                textMeshProUgui.text = $"{health.GetHealthPoints():0}-{health.GetMaxHealthPoints():0}";
            }
            else
                textMeshProUgui.text = "N/A";
        }
    }

}

