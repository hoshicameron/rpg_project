﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using RPG.Saving;
using UnityEngine;

namespace RPG.SceneManagement
{
    public class SavingWrapper : MonoBehaviour
    {
        [SerializeField] private float fadeInTime = 0.2f;
        private const string DefaultSaveFile = "Save";

        private void Awake()
        {
            StartCoroutine(nameof(LoadLastScene));
        }

        private IEnumerator  LoadLastScene()
        {
            Fader fader=FindObjectOfType<Fader>();
            SavingSystem savingSystem = GetComponent<SavingSystem>();
            fader.FadeOutImmidietly();
            yield return savingSystem.LoadLastScene(DefaultSaveFile);
            yield return fader.FadeIn(fadeInTime);
        }

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.L))
            {
                Load();
            }

            if (Input.GetKeyDown(KeyCode.S))
            {
                Save();
            }
            if (Input.GetKeyDown(KeyCode.Delete))
            {
                Delete();
            }
        }

        public void Save()
        {
            GetComponent<SavingSystem>().Save(DefaultSaveFile);
        }

        public void Load()
        {
            GetComponent<SavingSystem>().Load(DefaultSaveFile);
        }

        public void Delete()
        {
            GetComponent<SavingSystem>().Delete(DefaultSaveFile);
        }
    }
}
