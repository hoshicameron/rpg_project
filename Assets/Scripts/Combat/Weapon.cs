﻿using System.Collections;
using System.Collections.Generic;
using RPG.Core;
using RPG.Resources;
using UnityEngine;
using UnityEngine.Serialization;

namespace RPG.Combat
{

    [CreateAssetMenu(fileName = "Weapon", menuName = "Weapons/Make New Weapon", order = 0)]
    public class Weapon : ScriptableObject
    {
        [FormerlySerializedAs("weaponPrefab")] [SerializeField] private GameObject equippedPrefab = null;
        [SerializeField] private Projectile projectile = null;
        [SerializeField] private AnimatorOverrideController weaponOverrideController = null;
        [SerializeField] private float weaponRange = 2.0f;
        [SerializeField] private float weaponDamage = 5f;
        [SerializeField] private bool isRightHanded = true;

        private const string WeaponName = "Weapon";
        public float WeaponRange
        {
            get { return weaponRange; }
        }

        public float WeaponDamage
        {
            get { return weaponDamage; }
        }

        public void Spawn(Transform rightHand,Transform leftHand, Animator animator)
        {
            DestroyOldWeapon(rightHand,leftHand);
            if(equippedPrefab)
            {
                var handTransform = GetTransform(rightHand, leftHand);
                GameObject weapon=Instantiate<GameObject>(equippedPrefab, handTransform);
                weapon.name = WeaponName;
            }

            var overrideController = animator.runtimeAnimatorController;
            if(weaponOverrideController)
                animator.runtimeAnimatorController = weaponOverrideController;
            else if (overrideController)
                animator.runtimeAnimatorController = overrideController;
        }

        private void DestroyOldWeapon(Transform rightHand, Transform leftHand)
        {
            Transform oldWeapon = rightHand.Find(WeaponName);
            if (!oldWeapon)
                oldWeapon = leftHand.Find(WeaponName);
            if(!oldWeapon)    return;

            oldWeapon.name = "DESTROYING";
            Destroy(oldWeapon.gameObject);
        }

        private Transform GetTransform(Transform rightHand, Transform leftHand)
        {
            Transform handTransform = (isRightHanded) ? rightHand : leftHand;
            return handTransform;
        }

        public void LunchProjectile(Transform rightHand,Transform leftHand,Health target,GameObject instigator, float calculatedDamage)
        {
            if(!this.projectile)    return;

            Projectile projectileInstance = Instantiate(projectile, GetTransform(rightHand, leftHand).position,Quaternion.identity);
            projectileInstance.GetComponent<Projectile>().Target = target;
            projectileInstance.GetComponent<Projectile>().Damage = calculatedDamage;
            projectileInstance.GetComponent<Projectile>().Instigator = instigator;
        }

        public bool HasProjectile()
        {
            return (projectile) ? true : false;
        }
    }

}
