﻿using System.Collections.Generic;
using UnityEngine;

namespace RPG.Stats
{
    interface IModifierProvider
    {
        IEnumerable<float> GetAdditiveModifier(Stat stat);
    }
}
